[![pipeline status](https://gitlab.com/outeniqua/express-web-app/badges/master/pipeline.svg)](https://gitlab.com/outeniqua/express-web-app/commits/master)

# Boilerplate Express and MongoDb web app


### Edit or create a .env file in the project root with the following:


  ```CONNECTIONSTRING=<Your Connection String>```


 ```PORT=<Desired Port Number>```

### Update packages

 Execute ```npm install```

 ### Run locally

 Execute ```npm run db``` ***OR*** ```npm run watch```


 Navigate to localhost:```PORT```



Enjoy!
